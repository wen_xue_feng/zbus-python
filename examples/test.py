#encoding=utf8

import json
import datetime

class MyJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        elif isinstance(obj, datetime.date):
            return obj.isoformat()
        elif isinstance(obj, datetime.timedelta):
            return (datetime.datetime.min + obj).time().isoformat()
        return json.JSONEncoder.default(self, obj)


v = datetime.datetime.now()
res = json.dumps(v, cls=MyJsonEncoder)
print(res)