#encoding=utf8   
import sys
sys.path.append("../") 

from zbus.db import Db, config

                
from sqlalchemy import create_engine                   
                
engine = create_engine("mysql+pymysql://root:root@localhost/test?charset=utf8")

db = Db(engine)


with db.connection() as conn:
    res = conn.execute('select * from user2')
    for key in res.keys():
        print(key)
